TARGET := iphone:clang
TARGET_IPHONEOS_DEPLOYMENT_VERSION = 7.0
IHPONE_ARCHS = armv7 armv7s

include theos/makefiles/common.mk

TWEAK_NAME = nonSSL
nonSSL_FILES = Tweak.xm
nonSSL_FRAMEWORKS = Security
nonSSL_LIBRARIES = substrate

include $(THEOS_MAKE_PATH)/tweak.mk

