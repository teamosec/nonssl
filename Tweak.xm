//**********
//	For Debugging Only!
//**********

#import <Security/SecureTransport.h>
#import "substrate.h"

typedef OSStatus (*func_SSLSetSessionOption)(SSLContextRef context, SSLSessionOption option, BOOL value);
typedef OSStatus (*func_SSLHandshake)(SSLContextRef context);
typedef SSLContextRef (*func_SSLCreateContext)(CFAllocatorRef alloc, SSLProtocolSide protocolSide, SSLConnectionType 
connectionType);

NSString *startFile = @"/574R7.1337";

static BOOL hookIsEnabled(NSString *enablePath) {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	return [fileManager fileExistsAtPath:enablePath];
}

func_SSLSetSessionOption orig_SSLSetSessionOption = NULL;
func_SSLHandshake orig_SSLHandshake = NULL;
func_SSLCreateContext orig_SSLCreateContext = NULL;

//Set any ssl session with kSSLSessionOptionBreakOnServerAuth means every unauthorized certifecate 
//will be tested again by the application itself.
OSStatus hook_SSLSetSessionOption (SSLContextRef context, SSLSessionOption option, BOOL value) {
        if (!hookIsEnabled(startFile)){
		NSLog(@"File not fount in set session option!");
                return (*orig_SSLSetSessionOption)(context, option, value);
	}
	return (*orig_SSLSetSessionOption)(context, kSSLSessionOptionBreakOnServerAuth, value);
}

//When handshaking in case of using unauthorized certifecate handshake function will return 
// errSSLServerAuthCompleted value when the session option is set to kSSLSessionOptionBreakOnServerAuth
//and then if the application takes responsebilty on the certifecate it'll call the handshake again.
OSStatus hook_SSLHandshake (SSLContextRef context) {
	if (!hookIsEnabled(startFile)){
		NSLog(@"File not found in handshake!");
		return (*orig_SSLHandshake)(context);
	}
	OSStatus err = (*orig_SSLHandshake)(context);
	if (err == errSSLServerAuthCompleted) {
		return (*orig_SSLHandshake)(context);
	}
	else {
		return err;
	}
}

//When creating a session we will set the session option to accept application validation.
SSLContextRef hook_SSLCreateContext (CFAllocatorRef alloc, SSLProtocolSide protocolSide, SSLConnectionType 
connectionType) {
        if (!hookIsEnabled(startFile)){
		NSLog(@"File not found in CreateContext!");
        	return (*orig_SSLCreateContext)(alloc, protocolSide, connectionType);
        }
	SSLContextRef sslContext = (*orig_SSLCreateContext)(alloc, protocolSide, connectionType);
	(*orig_SSLSetSessionOption)(sslContext, kSSLSessionOptionBreakOnServerAuth, true);
	return sslContext;
}

%ctor {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
 
    // Set our hooks
    MSHookFunction((void *)SSLSetSessionOption, (void *)hook_SSLSetSessionOption, (void **)&orig_SSLSetSessionOption);
    MSHookFunction((void *)SSLCreateContext, (void *)hook_SSLCreateContext, (void **)&orig_SSLCreateContext);
    MSHookFunction((void *)SSLHandshake, (void *)hook_SSLHandshake, (void **)&orig_SSLHandshake);

    [pool drain];
}

