Nonssl

What is nonssl?
Nonssl is an injected dynamic library that cancel ssl certificate validation by accepting any certificate .

What it does?
Nonssl hooks three functions under SecureTransport framework, making validation optional by the calling application and accepting any.
(See Tweak.xm documentation)

Dependencies:
In this version iPhone needs to be jail broken so ״mobilesubstrate" will be installed.

Mobile substrate - hooking management library which automatically installed when jail breaking iPhone.

How to make it work,
There are two compiled files under nonssl/_/Library/MobileSubstrate/DynamicLibraries
Needs to be copied to /Library/MobileSubstrate/DynamicLibraries 
And from now and then any process loads SecureTransport framework will be hooked.

Optional future updates:
* on/off hidden switch 
* no MobileSubstrate dependency