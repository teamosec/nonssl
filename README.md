# Nonssl #

## What is nonssl? ##

Nonssl is an injected dynamic library that cancels ssl certificate validation by accepting any certificate .


## What it does? ##

Nonssl hooks three functions under SecureTransport framework, making validation optional by the calling application and accepting any.
(See Tweak.xm documentation)


### Dependencies: ###

In this version iPhone needs to be jail broken so ״mobilesubstrate" will be installed.


Mobile substrate - hooking management library which automatically installed when jail breaking iPhone.


## How to make it work? ##

There are two compiled files under nonssl/_/Library/MobileSubstrate/DynamicLibraries
Need to be copied to /Library/MobileSubstrate/DynamicLibraries 

Then when creating the "574R7.1337" file in the root path (can be easily changed) SecureTransport framework will be hooked.


### Optional future updates: ###
 
* no MobileSubstrate dependency.

----------------------------------------------------------------------------------------

# iIntercept #

iIntercept is an interactive testing tool for the nonssl tweak written in bash.
It needs to be used on linux machine that functions as a mitm point.

When giving ip address user name and password will insert the nonssl library to the MobileSubstrate path on the attacked iPhone by the "attack" command, and when running the "activate" command the start hooking file will be written to its path on the attacked iPhone too and this enables the nonSSL hooks on target.

and then when running the "poisn" command, iIntercept arpspoofs the target and the gateway then it makes all incoming and out coming data pass through the mitm point (where iIntercept is running).
 
* The iptables redirecting rules are set at the begining when running the script and redirecting ports 80 and 443 to localhost:8080.

 By running any ssl intercept program as gateway on port 8080 we can see all the ssl transport on plain text.

The "quit" command delets the iptables rules, stops the spoofing and exits.

Other commands usage can be viewed by the "help" command.

### dependencies: ###

* "arpspoof" tool
* "sshpass"